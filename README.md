- Spring Web : pour faire une API
- Spring Data JPa : pour intéragir avec la BDD
- H2 Database : c'est le SGBD pour ce projet (on aurait pu choisr MySQL ou Postgre par exemple)

Movie :
- movieId
- title

Director : plusieurs films peuvent avoir le même réalisateur, un même film n'aura qu'un seul réalisateur

Genre : un même genre pourra être attribué à plusieurs films, un films pourra posséder plusieurs genres

Endpoints du CRUD pour Movie :

- GET /movies afficher la liste des films
- GET /movies/:id récupérer un film grâce à son idenfiant
- POST /movies créer un film
- UPDATE /movies modifier un film
- DELETE /movies supprimer un film

- Controller : pour les routes
- Model : pour réprésenter les données
- Connexion à la base de données
- Requêtes SQL pour chaque action

ORM (Design Pattern) : Object Relation Mapping -> ça transforme un objet en données stockable dans une BDD, et inversement. Sequelize est un ORM pour JavaScript.

- JPA (Java Persistance API) : c'est la spécification d'un ORM en Java. Ce sont juste des interfaces (des signatures de méthodes).
- Hibernate : c'est un ORM, c'est une implémentation de JPA
- JDBC (Java DataBase Connector) : c'est une bibliothèque que sert à faire des requêtes SQL avec Java. Hibernate utilise JDBC.
- SPring Data JPA : surcouche de Spring pour utiliser Hibernate

- Entity (entité) : représentation d'une table en orienté objet. C'est un model, sauf que ça a un rapport avec une base de données
- Repository : ça permet d'interagir (ex: lire, créer, supprimer, modifier) avec la base de données (il faut lui spécifier l'entité avec laquelle on veut travailler)