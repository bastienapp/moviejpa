package com.example.moviejpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.moviejpa.entity.Movie;
import java.util.List;


// CrudRepository
// pourquoi Long et pas long
// qu'est-ce qu'un type générique / généricité
@Repository
public interface MovieRepo extends JpaRepository<Movie, Long> {

    // ici : j'ai du code uniquement si je veux ajouter mes propres requêtes

    // toutes les actions du CRUD sont héritées de JpaRepository

    // trouver une film par son titre
    // query method : hibernate génère la requête tout seul à partir du nom de la méthode : SELECT * FROM movie WHERE title LIKE %?%
    public List<Movie> findByTitleContainingIgnoreCase(String title);
}
