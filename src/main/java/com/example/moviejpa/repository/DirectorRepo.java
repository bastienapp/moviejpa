package com.example.moviejpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.moviejpa.entity.Director;

@Repository
public interface DirectorRepo extends JpaRepository<Director, Long> {


}
