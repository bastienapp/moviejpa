package com.example.moviejpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.moviejpa.entity.Genre;

@Repository
public interface GenreRepo extends JpaRepository<Genre, Long> {

}
