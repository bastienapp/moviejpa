package com.example.moviejpa.entity;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;

@Entity
// JavaBean : pour qu'Hibernate arrive à utiliser cette classe pour en faire une
// table et pour pouvoir manipuler l'objet
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // on met Long et pas long car l'identifiant peut être null, dans le cas où
    // l'entité n'a pas été enregistrée en base de données
    private Long movieId;

    private String title;

    @ManyToOne
    // @JoinColumn(name = "director_id") // si je veux donner un nom spécifique à ma
    // clé étrangère
    @JsonIgnoreProperties({ "movieList" })
    private Director director;

    @ManyToMany
    // @JoinTable : pour définir le nom de la table de jointure et des clés
    // étrangère
    @JoinTable(name = "movie_has_genre", joinColumns = @JoinColumn(name = "movie_id"), inverseJoinColumns = @JoinColumn(name = "genre_id"))
    @JsonIgnoreProperties({ "movieList" })
    Set<Genre> genreSet = new HashSet<>();

    public Movie() {
    }

    // getter et setter : utilisés par Hibernate pour générer un objet à partir
    // d'une requête
    public Long getMovieId() {
        return this.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Set<Genre> getGenreSet() {
        return this.genreSet;
    }

    public void setGenreSet(Set<Genre> genreSet) {
        this.genreSet = genreSet;
    }
}
