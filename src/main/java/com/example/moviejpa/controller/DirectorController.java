package com.example.moviejpa.controller;

import org.springframework.web.bind.annotation.RestController;

import com.example.moviejpa.entity.Director;
import com.example.moviejpa.repository.DirectorRepo;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class DirectorController {

    private DirectorRepo directorRepo;

    public DirectorController(DirectorRepo directorRepo) {
        this.directorRepo = directorRepo;
    }

    @GetMapping("/directors")
    public List<Director> getAllDirectors() {
        return this.directorRepo.findAll();
    }

}
