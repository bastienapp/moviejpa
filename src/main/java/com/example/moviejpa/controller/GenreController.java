package com.example.moviejpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.example.moviejpa.entity.Genre;
import com.example.moviejpa.repository.GenreRepo;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class GenreController {

    @Autowired
    private GenreRepo genreRepo;

    @GetMapping("/genres")
    public List<Genre> getAllGenres() {
        return this.genreRepo.findAll();
    }

}
