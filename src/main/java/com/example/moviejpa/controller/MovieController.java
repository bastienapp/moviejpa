package com.example.moviejpa.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.moviejpa.entity.Movie;
import com.example.moviejpa.service.MovieService;

import jakarta.persistence.EntityNotFoundException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PutMapping;



@RestController
@RequestMapping("/movies")
public class MovieController {

    // @Autowired : un autre façon de faire de l'injection de dépendance
    private MovieService movieService;

    public MovieController(MovieService movieServiceInjected) {
        this.movieService = movieServiceInjected;
    }

    @GetMapping("")
    public List<Movie> getMovieList() {

        List<Movie> allMovies = this.movieService.getAllMovies();

        return allMovies;
    }

    // ResponseEntity : gérer les code de retour dans l'API
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable(name = "id") Long movieId) {
        try {
            // return new ResponseEntity<>(this.movieService.getMovieById(movieId),
            // HttpStatus.OK);
            return ResponseEntity.ok(this.movieService.getMovieById(movieId));
        } catch (EntityNotFoundException exception) {
            return new ResponseEntity<Movie>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/search/{title}")
    public List<Movie> getMoviesByTitle(@PathVariable String title) {
        return this.movieService.getMoviesByTitle(title);
    }


    @PostMapping("")
    public Movie postMovie(@RequestBody Movie movie) {

        return this.movieService.createMovie(movie);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteMovieById(@PathVariable Long id) {
        try {
            this.movieService.deleteMovie(id);
            return ResponseEntity.ok(true);
        } catch (EntityNotFoundException exception) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movieDataToUpdate) {

        try {
            return ResponseEntity.ok(this.movieService.updateMovie(id, movieDataToUpdate));
        } catch (EntityNotFoundException exception) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
