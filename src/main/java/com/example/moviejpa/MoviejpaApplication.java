package com.example.moviejpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviejpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviejpaApplication.class, args);
	}

}
