package com.example.moviejpa.service;


import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.moviejpa.entity.Movie;
import com.example.moviejpa.repository.MovieRepo;

import jakarta.persistence.EntityNotFoundException;

@Service
public class MovieService {

    private MovieRepo movieRepo;

    public MovieService(MovieRepo movieRepoInjected) {
        this.movieRepo = movieRepoInjected;
    }

    public List<Movie> getAllMovies() {

        return this.movieRepo.findAll();
    }

    public List<Movie> getMoviesByTitle(String title) {

        return this.movieRepo.findByTitleContainingIgnoreCase(title);
    }

    public Movie getMovieById(Long id) {

        Optional<Movie> optionalMovie = this.movieRepo.findById(id);

        // si j'ai bien récupéré un film
        if (optionalMovie.isPresent()) {
            //  si j'ai un film, je peux le récupérer avec .get()
            return optionalMovie.get();
        } else {
            // pas de film, je lève une exception
            throw new EntityNotFoundException("No movie found with id " + id);
        }
    }

    public Movie createMovie(Movie newMovie) {

        return this.movieRepo.save(newMovie);
    }

    public void deleteMovie(Long id) {

        Optional<Movie> optionalMovie = this.movieRepo.findById(id);
        if (optionalMovie.isPresent()) {
            this.movieRepo.deleteById(id);
        } else {
            throw new EntityNotFoundException("No movie found with id " + id);
        }
    }

    public Movie updateMovie(Long movieId, Movie movieData) {
        // je récupère l'indentifiant du film à partir des paramètres

        // si l'entité a un identifiant : il l'a met à jour
        Optional<Movie> optionalMovie = this.movieRepo.findById(movieId);
        if (optionalMovie.isPresent()) {
            Movie movieUpdated = optionalMovie.get();
            movieUpdated.setMovieId(movieId);
            movieUpdated.setTitle(movieData.getTitle());
            // TODO validation des données
            return this.movieRepo.save(movieUpdated);
        } else {
            throw new EntityNotFoundException("No movie found with id " + movieId);
        }
    }
}
